﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trecco.Dominio.Entidades;
using Trecco.Dominio.Servicos;

namespace Trecco.Controllers
{
    public class GruposController : Controller
    {
        private readonly IGruposService service;

        public GruposController()
        {
            service = new GruposService();
        }

        public ActionResult List(int idQuadro)
        {
            var grupos = service.ObterAtivos(new QuadroID(idQuadro));

            ViewBag.Quadro = service.ObterQuadro(new QuadroID(idQuadro));

            return View(grupos);
        }

        public ActionResult Create(int idQuadro)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(int idQuadro, FormCollection formulario)
        {
            var nomeGrupo = formulario["txtNomeGrupo"];

            service.Incluir(nomeGrupo, new QuadroID(idQuadro));

            return RedirectToAction("List");
        }

        public ActionResult Edit(int idQuadro, int id)
        {
            var grupo = service.Obter(new GrupoID(id));

            return View(grupo);
        }

        [HttpPost]
        public ActionResult Edit(int idQuadro, int id, FormCollection formulario)
        {
            var nomeGrupo = formulario["txtNomeGrupo"];

            service.Alterar(new GrupoID(id), nomeGrupo);

            return RedirectToAction("List");
        }

        public ActionResult MoveLeft(int idQuadro, int id)
        {
            service.MoverParaAEsquerda(new QuadroID(idQuadro), new GrupoID(id));
            return RedirectToAction("List");
        }

        public ActionResult MoveRight(int idQuadro, int id)
        {
            service.MoverParaADireita(new QuadroID(idQuadro), new GrupoID(id));

            return RedirectToAction("List");
        }

        public ActionResult Delete(int idQuadro, int id)
        {
            service.Remover(new GrupoID(id));

            return RedirectToAction("List", new { idQuadro });
        }

    }
}