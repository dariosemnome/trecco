﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trecco.Dominio.Entidades;
using Trecco.Dominio.Servicos;
using Trecco.ViewModels.Quadros.Forms;

namespace Trecco.Controllers
{
    public class QuadrosController : Controller
    {
        private readonly IQuadrosService service;

        public QuadrosController()
        {
            this.service = new QuadrosService();
        }

        public ActionResult List()
        {
            var quadros = service.ObterQuadros();

            return View(quadros);
        }  

        public ActionResult Delete(int id)
        {
            service.InativarQuadro(new QuadroID(id));

            return RedirectToAction("list");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(QuadroForm formulario)
        {
            if (ModelState.IsValid)
            {
                service.CriarQuadro(formulario.Nome);

                TempData["MensagemSucesso"] = "Quadro criado!";

                return RedirectToAction("List");
            }
            else
            {
                return View(formulario);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var quadro = service.ObterQuadro(new QuadroID(id));

            if (quadro != null)
                return View(
                    new QuadroForm
                    {
                        Nome = quadro.Nome
                    });
            else
                return HttpNotFound("Quadro não encontrado");
        }

        [HttpPost]
        public ActionResult Edit(int id, QuadroForm formulario)
        {
            service.AtualizarQuadro(new QuadroID(id), formulario.Nome);

            TempData["MensagemSucesso"] = "Quadro atualizado!";

            return RedirectToAction("List");
        }
    }
}