﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Trecco.ViewModels.Quadros.Forms
{
    public class QuadroForm
    {
        [DisplayName("Nome")]
        [Required(ErrorMessage = "O nome do quadro é obrigatório.")]
        [MaxLength(100, ErrorMessage ="O nome deve ser menor que 100 caracteres.")]
        public string Nome { get; set; }

        [DisplayName("Descrição")]
        [MaxLength(500, ErrorMessage = "A descrição deve ser menor que 500 caracteres.")]
        public string Descricao { get; set; }
    }
}