﻿using System;
using System.Collections.Generic;
using System.Linq;
using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Repositorios
{
    public class GruposEmMemoriaRepository : IGruposRepository
    {
        private static Dictionary<GrupoID, Grupo> GruposDictionary = new Dictionary<GrupoID, Grupo>();
        //private static List<Grupo> Grupos = new List<Grupo>();
        private static int ultimoId = 0;

        public GrupoID ObterProximoID()
        {
            ultimoId++;
            return new GrupoID(ultimoId);
        }

        public void Salvar(Grupo grupo)
        {
            var grupoId = grupo.Id;

            if (GruposDictionary.ContainsKey(grupoId))
                GruposDictionary[grupoId] = grupo;
            else
                GruposDictionary.Add(grupoId, grupo);
        }

        public int ObterMaximaPosicaoGrupo(QuadroID idQuadro) =>
            GruposDictionary.Values
            .Where(grupo => grupo.Quadro.Id.Equals(idQuadro))
            .Select(grupo => grupo.Posicao)
            .DefaultIfEmpty(0)
            .Max();

        public IEnumerable<Grupo> ObterGrupos(QuadroID idQuadro) =>
            from grupo in GruposDictionary.Values
            where grupo.Quadro.Id.Equals(idQuadro)
            orderby grupo.Posicao descending
            select grupo;

        public Grupo Obter(GrupoID id) =>
            GruposDictionary.Values.Where(grupo => grupo.Id.Equals(id)).FirstOrDefault();

        public Grupo ObterGrupoMenorPosicao(int posicao) =>
            (from grupo in GruposDictionary.Values
             where grupo.Posicao < posicao
             orderby grupo.Posicao descending
             select grupo).FirstOrDefault();

        public Grupo ObterGrupoMaiorPosicao(int posicao) =>
            (from grupo in GruposDictionary.Values
             where grupo.Posicao > posicao
             orderby grupo.Posicao
             select grupo).FirstOrDefault();

        public void Remover(GrupoID id) => GruposDictionary.Remove(id);
    }
}