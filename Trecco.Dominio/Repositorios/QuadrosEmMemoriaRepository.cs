﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Repositorios
{
    public class QuadrosEmMemoriaRepository : IQuadrosRepository
    {
        private static IDictionary<QuadroID, Quadro> QuadrosDictionary = new Dictionary<QuadroID, Quadro>();
        private static int ultimoId = 0;

        public QuadroID ObterProximoID()
        {
            ultimoId++;
            return new QuadroID(ultimoId);
        }

        public void Salvar(Quadro quadro)
        {
            var quadroId = quadro.Id;

            if (QuadrosDictionary.ContainsKey(quadroId))
                QuadrosDictionary[quadroId] = quadro;
            else
                QuadrosDictionary.Add(quadroId, quadro);
        }

        public void Inativar(QuadroID id)
        {
            var quadroARemover = this.Obter(id);

            if (quadroARemover != null)
                quadroARemover.Inativar();
            else
                throw new ArgumentException($"O elemento com ID {id} não existe");
        }

        public bool ExcluirPermanentemente(QuadroID id) => QuadrosDictionary.Remove(id);

        public Quadro Obter(QuadroID id) => 
            QuadrosDictionary[id];

        public IEnumerable<Quadro> ObterTodos() => 
            QuadrosDictionary.Values.OrderBy(quadro => quadro.Nome);

        public IEnumerable<Quadro> ObterAtivos() => 
            QuadrosDictionary.Values.Where(quadro => quadro.Ativo).OrderBy(quadro => quadro.Nome);

        public IEnumerable<Quadro> ObterInativos() => 
            QuadrosDictionary.Values.Where(quadro => !quadro.Ativo).OrderBy(quadro => quadro.Nome);

        public void Restaurar(QuadroID id)
        {
            var quadroARestaurar = this.Obter(id);

            if (quadroARestaurar != null)
                quadroARestaurar.Restaurar();
            else
                throw new ArgumentException($"O elemento com ID {id} não existe");
        }        
    }
}