﻿using System.Collections.Generic;
using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Repositorios
{
    public interface IGruposRepository
    {
        Grupo Obter(GrupoID id);
        Grupo ObterGrupoMaiorPosicao(int posicao);
        Grupo ObterGrupoMenorPosicao(int posicao);
        IEnumerable<Grupo> ObterGrupos(QuadroID idQuadro);
        int ObterMaximaPosicaoGrupo(QuadroID idQuadro);
        GrupoID ObterProximoID();
        void Remover(GrupoID id);
        void Salvar(Grupo grupo);
    }
}