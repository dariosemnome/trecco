﻿using System.Collections.Generic;
using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Servicos
{
    public interface IGruposService
    {
        void Alterar(GrupoID id, string novoNome);
        void Incluir(string nome, QuadroID idQuadro);
        void MoverParaADireita(QuadroID idQuadro, GrupoID id);
        void MoverParaAEsquerda(QuadroID idQuadro, GrupoID id);
        Grupo Obter(GrupoID id);
        IEnumerable<Grupo> ObterAtivos(QuadroID idQuadro);
        Quadro ObterQuadro(QuadroID idQuadro);
        void Remover(GrupoID id);
    }
}