﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Trecco.Dominio.Entidades;
using Trecco.Dominio.Repositorios;

namespace Trecco.Dominio.Servicos
{
    public class QuadrosService : IQuadrosService
    {
        private IQuadrosRepository _quadrosRepository;

        public QuadrosService()
        {
            //TODO: Alterar utilização para remover a dependencia do QuadrosEmMemoriaRepository
            _quadrosRepository = new QuadrosEmMemoriaRepository();
        }

        public Quadro ObterQuadro(QuadroID quadroID)
        {
            var quadro = _quadrosRepository.Obter(quadroID);
            return quadro;
        }

        public IEnumerable<Quadro> ObterQuadros()
        {
            return _quadrosRepository.ObterAtivos();
        }

        public IEnumerable<Quadro> ObterQuadrosInativos()
        {
            return _quadrosRepository.ObterInativos();
        }

        public void CriarQuadro(string nome)
        {
            var novoId = _quadrosRepository.ObterProximoID();

            var novoQuadro = new Quadro(novoId, nome);

            _quadrosRepository.Salvar(novoQuadro);
        }

        public void AtualizarQuadro(QuadroID quadroID, string novoNome)
        {
            var quadro = _quadrosRepository.Obter(quadroID);

            quadro.AlterarNome(novoNome);

            _quadrosRepository.Salvar(quadro);
        }

        public void InativarQuadro(QuadroID quadroID)
        {
            var quadro = _quadrosRepository.Obter(quadroID);
            quadro.Inativar();
            _quadrosRepository.Salvar(quadro);
        }

        public void ReativarQuadro(QuadroID quadroID)
        {
            var quadro = _quadrosRepository.Obter(quadroID);
            quadro.Restaurar();
            _quadrosRepository.Salvar(quadro);
        }

        public bool ExcluirPermanentemente(QuadroID quadroID)
        {
            return _quadrosRepository.ExcluirPermanentemente(quadroID);
        }
    }
}
