﻿using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Servicos
{
    public interface IUsuariosService
    {
        bool ExisteUsuario(string email);

        Usuario CriarUsuario(string email, string nome, string sobrenome, string senha);
        Usuario ObterUsuario(string email);
    }
}