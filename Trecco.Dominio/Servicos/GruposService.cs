﻿using System;
using System.Collections.Generic;
using Trecco.Dominio.Entidades;
using Trecco.Dominio.Repositorios;

namespace Trecco.Dominio.Servicos
{
    public class GruposService : IGruposService
    {
        private IGruposRepository _gruposRepository;
        private IQuadrosRepository _quadrosRepository;

        public GruposService()
        {
            _gruposRepository = new GruposEmMemoriaRepository();
            _quadrosRepository = new QuadrosEmMemoriaRepository();

        }

        public void Incluir(string nome, QuadroID idQuadro)
        {
            var quadro = _quadrosRepository.Obter(idQuadro);

            if(quadro != null)
            {
                //var gruposRepository = new GruposEmMemoriaRepository();
                var ultimaPosicao = _gruposRepository.ObterMaximaPosicaoGrupo(idQuadro);

                var proximoId = _gruposRepository.ObterProximoID();

                var novoGrupo = new Grupo(proximoId, nome, ultimaPosicao + 1, quadro);
                _gruposRepository.Salvar(novoGrupo);
            }
            else
            {
                throw new ArgumentException("O quadro especificado não foi encontrado");
            }
        }

        public IEnumerable<Grupo> ObterAtivos(QuadroID idQuadro)
        {
            return _gruposRepository.ObterGrupos(idQuadro);
        }

        public Grupo Obter(GrupoID id)
        {
            return _gruposRepository.Obter(id);
        }

        public Quadro ObterQuadro(QuadroID idQuadro)
        {
            var quadro = _quadrosRepository.Obter(idQuadro);
            return quadro;
        }

        public void Remover(GrupoID id)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            gruposRepository.Remover(id);
        }

        public void Alterar(GrupoID id, string novoNome)
        {
            var grupo = _gruposRepository.Obter(id);

            grupo.AlterarNome(novoNome);
            _gruposRepository.Salvar(grupo);
        }

        public void MoverParaADireita(QuadroID idQuadro, GrupoID id)
        {
            var grupo = _gruposRepository.Obter(id);
            if (grupo.Posicao > 1)
            {
                var grupoADireita = _gruposRepository.ObterGrupoMenorPosicao(grupo.Posicao);
                this.SwapGrupos(grupo, grupoADireita);
            }
        }

        public void MoverParaAEsquerda(QuadroID idQuadro, GrupoID id)
        {
            var grupo = _gruposRepository.Obter(id);
            if (grupo.Posicao < _gruposRepository.ObterMaximaPosicaoGrupo(idQuadro))
            {
                var grupoAEsquerda = _gruposRepository.ObterGrupoMaiorPosicao(grupo.Posicao);

                this.SwapGrupos(grupo, grupoAEsquerda);
            }
        }

        private void SwapGrupos(Grupo grupoA, Grupo grupoB)
        {
            grupoA.AlterarPosicao(novaPosicao: grupoB.Posicao);
            grupoB.AlterarPosicao(novaPosicao: grupoA.Posicao);

            _gruposRepository.Salvar(grupoA);
            _gruposRepository.Salvar(grupoB);
        }
    }
}