﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trecco.Dominio.Entidades
{
    public class Usuario
    {
        public string Email { get; private set; }
        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }
        public string Senha { get; private set; }
        public DateTime DataCriacao { get; private set; }
        public DateTime DataAlteracao { get; private set; }

        private Usuario() { }

        public static Usuario Create(string email, string nome, string sobrenome, String senha)
        {
            var novoUsuario = new Usuario {
                Email = email,
                Nome = nome,
                Sobrenome = sobrenome,
                Senha = senha,
                DataCriacao = DateTime.Now,
                DataAlteracao = DateTime.Now
            };

            return novoUsuario;
        }
    }
}
