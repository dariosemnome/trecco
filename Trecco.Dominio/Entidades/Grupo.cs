﻿using System;

namespace Trecco.Dominio.Entidades
{
    /// <summary>
    /// Esta estrutura é para representar um ID de quadro. A ideia aqui é prevenir que eu utilize um método
    /// que deveria utilizar um ID de quadro, passando um outro valor qualquer. Um exemplo disso acontecendo
    /// é se o desenvolvedor, por engano, passa um quadroID ao invés de um GrupoID no método que alterna a
    /// posição dos grupos de um determinado quadro (no gruposService).
    /// Um Struct, no caso é um tipo de "classe leve". Representa um tipo de dado composto, porém, diferente de
    /// uma classe, que trabalha utilizando referências, um Struct usa passagem por valor, ou seja, se comporta
    /// de forma mais próxima a um int do que a um objeto.
    /// </summary>
    public struct GrupoID
    {
        //Se eu não defino o set em uma propriedade, seu valor só pode ser atribuído no construtor.
        public int Valor { get; }
        public GrupoID(int valor)
        {
            this.Valor = valor;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType() == this.GetType())
                return ((GrupoID)obj).Valor == this.Valor;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return 991088425 + Valor.GetHashCode();
        }

        public override string ToString()
        {
            return $"GrupoID({this.Valor})";
        }
    }

    public class Grupo
    {
        public GrupoID Id { get; private set; }
        public string Nome { get; private set; }
        public DateTime DataDeCriacao { get; }
        public DateTime DataDeAlteracao { get; private set; }
        public Quadro Quadro { get; private set; }
        public int Posicao { get; private set; }

        public Grupo()
        {
            this.DataDeCriacao = DateTime.Now;
            this.DataDeAlteracao = DateTime.Now;
        }

        public Grupo(GrupoID id, string nome, int posicao, Quadro quadro) : this()
        {
            this.Id = id;
            this.Nome = nome;
            this.Quadro = quadro;
            this.Posicao = posicao;
        }

        public void AlterarNome(string novoNome)
        {
            this.Nome = novoNome;
            this.DataDeAlteracao = DateTime.Now;
        }

        public void AlterarPosicao(int novaPosicao)
        {
            this.Posicao = novaPosicao;
            this.DataDeAlteracao = DateTime.Now;
        }
    }
}