﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trecco.Dominio.Servicos;

namespace Trecco.Dominio.Fabricas
{
    public class UsuariosServiceFactory
    {
        public static IUsuariosService CriarUsuariosService()
        {
            //TODO: Isto será alterado para chavear entre tipos diferentes
            return new UsuariosService();
        }
    }
}
